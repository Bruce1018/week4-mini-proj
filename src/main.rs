use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use std::str::FromStr;

mod lib;
use crate::lib::fibonacci;

async fn fibonacci_service(path: web::Path<String>) -> impl Responder {
    match usize::from_str(&path) {
        Ok(n) => {
            let fib = fibonacci(n);
            HttpResponse::Ok().body(format!("Fibonacci number at position {} is: {}", n, fib))
        }
        Err(_) => HttpResponse::BadRequest().body("Invalid number"),
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/fibonacci/{number}", web::get().to(fibonacci_service))
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
