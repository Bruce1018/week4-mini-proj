# week4-mini-proj

## Setup
- make sure you have `docker` in your device, if not , run the following command 
    - `brew install docker`
- create a blank project on `GitLab` and clone it 
- inside the directory, run `cargo init`
- in `Cargo.toml` file, add following dependencies
    - actix-web = "4.0"
    - actix-rt = "2.5"

## Step 1: Finish the function on `localhost`
1. edit `main.rs` to run a simple web server on `localhost`, you should see
``` rust
use actix_web::{web, App, HttpResponse, HttpServer, Responder};

async fn greet() -> impl Responder {
    HttpResponse::Ok().body("Hello, Actix!")
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new().route("/", web::get().to(greet))
    })
    .bind("127.0.0.1:8080")?
    .run()
    .await
}
```

![截屏2024-02-23 21.03.47.png](https://p9-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/2f0e41715a274802a2f8fb8e35d4b9b6~tplv-k3u1fbpfcp-jj-mark:0:0:0:0:q75.image#?w=688&h=408&s=28875&e=png&b=101010)

2. make the funtion more complicated, first, we create `lib.rs` and implement a `fib` algorithm

``` rust
pub fn fibonacci(n: usize) -> usize {
    let mut dp = vec![0; n + 1];
    dp[0] = 0;
    if n > 0 {
        dp[1] = 1;
        for i in 2..=n {
            dp[i] = dp[i - 1] + dp[i - 2];
        }
    }
    dp[n]
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_fibonacci() {
        assert_eq!(fibonacci(0), 0);
        assert_eq!(fibonacci(1), 1);
        assert_eq!(fibonacci(2), 1);
        assert_eq!(fibonacci(3), 2);
        assert_eq!(fibonacci(4), 3);
        assert_eq!(fibonacci(5), 5);
    }
}
```
make sure run `cargo test` and see following


![截屏2024-02-23 21.03.20.png](https://p9-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/c314b466697a40d48652684c671ec685~tplv-k3u1fbpfcp-jj-mark:0:0:0:0:q75.image#?w=1684&h=740&s=148927&e=png&b=181818)

3. update `main.rs` and check the local host again

``` rust
use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use std::str::FromStr; 


mod lib;
use crate::lib::fibonacci;

async fn fibonacci_service(path: web::Path<String>) -> impl Responder {

    match usize::from_str(&path) {
        Ok(n) => {

            let fib = fibonacci(n);
            HttpResponse::Ok().body(format!("Fibonacci number at position {} is: {}", n, fib))
        }
        Err(_) => HttpResponse::BadRequest().body("Invalid number"),
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/fibonacci/{number}", web::get().to(fibonacci_service))
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
```
use the following url to run the test
`localhost:8080/fibonacci/7`

![截屏2024-02-23 21.06.43.png](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/e368c16310334a2b956269eefb676289~tplv-k3u1fbpfcp-jj-mark:0:0:0:0:q75.image#?w=910&h=390&s=42189&e=png&b=111111)

## Step 2: Build Docker
1. download `docker desktop` from website
2. create `Dockerfile` and write following

``` makefile
# Build stage
FROM rust:1.68 AS build
WORKDIR /app
COPY . .
RUN cargo build --release

FROM gcr.io/distroless/cc-debian11
COPY --from=build /app/target/release/week4-mini-proj /app/

# use non-root user
USER nonroot:nonroot

# Set up App directory
ENV APP_HOME=/app
WORKDIR $APP_HOME

# Expose the port
EXPOSE 8080

# Run the web service on container startup.
ENTRYPOINT [ "/app/week4-mini-proj" ]
```

3. build docker image with this command 

`docker build -t my-rust-app .`

4. Then you can see on the `docker desktop`


![截屏2024-02-23 22.53.13.png](https://p1-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/d2311022c46c418590d03df78e7a13f5~tplv-k3u1fbpfcp-jj-mark:0:0:0:0:q75.image#?w=1892&h=220&s=41553&e=png&b=151b1e)

or you can runn `docker ps` to see it

![截屏2024-02-23 22.53.43.png](https://p1-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/38224c185f7e4562806297f102aa279c~tplv-k3u1fbpfcp-jj-mark:0:0:0:0:q75.image#?w=2042&h=150&s=58399&e=png&b=181818)

5. run the following command


`docker run -p 8080:8080 my-rust-app`

and check this url `http://localhost:8080/fibonacci/7`

you will see


![截屏2024-02-23 22.04.13.png](https://p1-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/6373af00b45c4835979bcd29646bb75c~tplv-k3u1fbpfcp-jj-mark:0:0:0:0:q75.image#?w=1056&h=440&s=44668&e=png&b=111111)