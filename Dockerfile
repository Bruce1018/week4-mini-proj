# Build stage
FROM rust:1.68 AS build
WORKDIR /app
COPY . .
RUN cargo build --release

FROM gcr.io/distroless/cc-debian11
COPY --from=build /app/target/release/week4-mini-proj /app/

# use non-root user
USER nonroot:nonroot

# Set up App directory
ENV APP_HOME=/app
WORKDIR $APP_HOME

# Expose the port
EXPOSE 8080

# Run the web service on container startup.
ENTRYPOINT [ "/app/week4-mini-proj" ]
